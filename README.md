# GamePush - Unity Plugin 


## Download:
[GamePush v1.1.0](https://gitlab.com/shagidullin/gamepush_unity/-/blob/master/Releases/GamePush%20v1.1.0.unitypackage)

##

## Documentation:

### English:
https://docs.gamepush.com/docs/get-start/


### Russian:
https://docs.gamepush.com/ru/docs/get-start/

##


## Using

```sh
# todo
```

## Development

### Install

Install [nodejs](https://nodejs.org/en/)

```sh 
# install dependencies
npm i
```

### Dev


```sh 
# start dev on https://localhost:8888
npm run dev
```

### Build


```sh 
# Create minified js version and create .unitypackage
# out >> builds/gamescore-vX.X.X.unitypackage
npm run build
```
